package com.hw.db.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import com.hw.db.models.Forum;
import com.hw.db.DAO.UserDAO;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.models.User;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessResourceFailureException;
import org.mockito.MockedStatic;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertEquals;

class forumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("Create forum test")
    void createForumTest() {
        loggedIn = new User("user", "user@gmail.com", "name", "empty");
        toCreate = new Forum(12, "user", 3, "title", "something");
    }

    @Test
    @DisplayName("Failure on forum creation as it already exists")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
            uMock.when(() -> UserDAO.Search("something")).thenReturn(loggedIn);

            try (MockedStatic<ForumDAO> fMock = Mockito.mockStatic(ForumDAO.class)) {
                fMock.when(() -> ForumDAO.Info(toCreate.getSlug())).thenReturn(toCreate);
                fMock.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DuplicateKeyException("Already existing forum"));

                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(toCreate), controller.create(toCreate), "Result on on forum creation when it already exists");
            }
            assertEquals(loggedIn, UserDAO.Search("something"));
        }
    }

    @Test
    @DisplayName("Non-authorized user failure while creating forum")
    void noLoginWhenCreatesForum() {
        try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
            uMock.when(() -> UserDAO.Search("something")).thenReturn(null);

            forumController controller = new forumController();
            assertEquals(HttpStatus.NOT_FOUND, controller.create(toCreate).getStatusCode(), "Result when non-authorised user fails to create a forum");
        }
    }

    @Test
    @DisplayName("Create forum test (OK)")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
            uMock.when(() -> UserDAO.Search("user")).thenReturn(loggedIn);

            try (MockedStatic<ForumDAO> fMock = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Results on successful forum creation");
            }

            assertEquals(loggedIn, UserDAO.Search("something"));
        }
    }

    @Test
    @DisplayName("Fail on forum creation as server cannot access DB")
    void conflictWhenCreatesForumBecauseOfResource() {
        try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
            uMock.when(() -> UserDAO.Search("something")).thenReturn(loggedIn);

            try (MockedStatic<ForumDAO> fMock = Mockito.mockStatic(ForumDAO.class)) {
                fMock.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessResourceFailureException("Already existing forum"));
                forumController controller = new forumController();

                controller.create(toCreate);
                assertEquals(HttpStatus.NOT_ACCEPTABLE, controller.create(toCreate).getStatusCode(), "Result for DB error while server creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }


}
