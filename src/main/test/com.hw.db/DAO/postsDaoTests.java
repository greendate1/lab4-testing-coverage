package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import com.hw.db.models.Post;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.sql.Timestamp;

@DisplayName("postDAO")
public class postsDaoTests {
    static Post getOldPost() {
        return new Post("nikola", new Timestamp(157894431), "random-forum", "random-message", 0, 0, false);
    }

    @Test
    @DisplayName("No changes")
    void testSetPost1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = getOldPost();

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        );
    }

    @Test
    @DisplayName("Change of message")
    void testSetPost5() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("nikola", new Timestamp(157894431), "random-forum", "other-random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change of timestamp")
    void testSetPost4() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("nikola", new Timestamp(157894556), "random-forum", "random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change of author")
    void testSetPost3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("marcus", new Timestamp(157894431), "random-forum", "random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change of message and timestamp")
    void testSetPost7() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("nikola", new Timestamp(157894556), "random-forum", "other-random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change of timestamp and author")
    void testSetPost6() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("marcus", new Timestamp(157894556), "", "random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }


    @Test
    @DisplayName("Change of message and author")
    void testSetPost8() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("marcus", new Timestamp(157894431), "random-forum", "other-random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Complete change")
    void testSetPost2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("marcus", new Timestamp(157894556), "random-forum", "other-random-message", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }
}
