package com.hw.db.DAO;

import org.mockito.Mockito;
import com.hw.db.DAO.ThreadDAO;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import com.hw.db.DAO.PostDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {

    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }


    @Test
    @DisplayName("Full statement tree sort coverage 1")
    void TreeSortTest2() {
        ThreadDAO.treeSort(0,null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch DESC ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Full statement tree sort coverage 2")
    void TreeSortTest1() {
        ThreadDAO.treeSort(0,null, null, false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }

}
