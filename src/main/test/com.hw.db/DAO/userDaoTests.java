package com.hw.db.DAO;

import com.hw.db.DAO.UserDAO;
import org.mockito.Mockito;
import org.testng.annotations.Test;
import com.hw.db.models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.DisplayName;

import static org.mockito.Mockito.*;

class userDaoTests {

    @Test
    @DisplayName("Edit using email")
    void ChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", "nikola@gmail.com", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
  @DisplayName("Edit using fullname")
  void ChangeTest6() {
      JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
      UserDAO userDAO = new UserDAO(mockJdbc);
      User user = new User("nikola", null, "Nikola", null);
      UserDAO.Change(user);
      verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));

    @Test
    @DisplayName("Edit using about")
    void ChangeTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", null, null, "Novarlic");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }


    @Test
    @DisplayName("Edit using fullname and about")
    void ChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", null, "Nikola", "Novarlic");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Edit using email and about")
    void ChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", "nikola@gmail.com", null, "Novarlic");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Edit using email and fullname")
    void ChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", "nikola@gmail.com", "Nikola", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

  @Test
  @DisplayName("Edit using all values")
  void ChangeTest1() {
      JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
      UserDAO userDAO = new UserDAO(mockJdbc);
      User user = new User("nikola", "nikola@gmail.com", "Nikola", "Novarlic");
      UserDAO.Change(user);
      verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
  }

    @Test
    @DisplayName("Nothing changes")
    void ChangeTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("nikola", null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
}
