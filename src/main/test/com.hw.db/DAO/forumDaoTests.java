package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;

@DisplayName("forumDAO")
public class forumDaoTests {

  @Test
  @DisplayName("Branch user list 1")
  void testUserlist2() {
      JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
      ForumDAO forum = new ForumDAO(mJdbc);
      UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
      ForumDAO.UserList("token", 10, "02.06.1998", true);
      verify(mJdbc).query(
              Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
              Mockito.any(Object[].class),
              Mockito.any(UserDAO.UserMapper.class)
      );
  }

  @Test
  @DisplayName("Branch user list 2")
  void testUserlist3() {
      JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
      ForumDAO forum = new ForumDAO(mJdbc);
      UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
      ForumDAO.UserList("token", 10, "02.06.1998", false);
      verify(mJdbc).query(
              Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
              Mockito.any(Object[].class),
              Mockito.any(UserDAO.UserMapper.class)
      );
  }

    @Test
    @DisplayName("Branch user list 3")
    void testUserlist1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("token", null, null, null);
        verify(mJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" + "::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 1")
    void testThreadList4() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",null, "02.06.1998", true);
        verify(mJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 2")
    void testThreadList3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",100, "02.06.1998", true);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 3")
    void testThreadList6() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",100, "02.06.1998", false);
        verify(mJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 4")
    void testThreadList1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",null, null, false);
        verify(mJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 5")
    void testThreadList2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",null, "02.06.1998", false);
        verify(mJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc thread list 6")
    void testThreadList5() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("token",100, null, true);
        verify(mJdbc).query(
                Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }


}
